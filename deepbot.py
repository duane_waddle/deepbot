#! /usr/bin/env python
#
# Karma pimper based on ircclient2.py from Joel Rosdahl <joel@rosdahl.net>
#


import irc.client
import sys
import logging
import re
import time


logging.basicConfig(level='DEBUG')

class IRCCat(irc.client.SimpleIRCClient):

    lastchanmsg = 0
    lastjoinmsg = 0

    def __init__(self, target):
        irc.client.SimpleIRCClient.__init__(self)
        self.target = target

    def upgoater(self):
        if (int(time.time()) - self.lastchanmsg > 3600) and ( int(time.time()) - self.lastjoinmsg > 180 ):
		self.connection.privmsg(self.target, "!bob_deep++")

    def on_welcome(self, connection, event):
        if irc.client.is_channel(self.target):
            connection.join(self.target)
	    self.lastchanmsg=int(time.time())
            self.reactor.execute_every(3,self.upgoater)

    def on_join(self, connection, event):
	self.lastjoinmsg=int(time.time())
	

    def on_pubmsg(self,connection,event):
	if re.search("Karma for \x02bob_deep\x02 is now",event.arguments[0]):
		pass
	if re.search("\!bob_deep\+\+",event.arguments[0]):
		pass
	elif re.search("duckbot",event.arguments[0]):
		self.connection.privmsg(self.target,"... okay, bye ... :( ")
		sys.exit(0)
	else:
		self.lastchanmsg=int(time.time())

    def on_disconnect(self, connection, event):
        sys.exit(0)


def main():
    if len(sys.argv) != 4:
        print("Usage: irccat2 <server[:port]> <nickname> <target>")
        print("\ntarget is a nickname or a channel.")
        sys.exit(1)

    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print("Error: Erroneous port.")
            sys.exit(1)
    else:
        port = 6667
    nickname = sys.argv[2]
    target = sys.argv[3]

    c = IRCCat(target)
    try:
        c.connect(server, port, nickname)
    except irc.client.ServerConnectionError as x:
        print(x)
        sys.exit(1)
    c.start()

if __name__ == "__main__":
    main()
